import random
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from rest_framework import serializers

class CustomUserManager(BaseUserManager):
    def create_user(self,email,password,first_name,last_name):
        if not email:
            raise ValueError("Email can not be blank!")
        if not password:
            raise ValueError("Password can not be blank!")
        if not first_name:
            raise ValueError("First name can not be blank!")
        if not last_name:
            raise ValueError("Surname can not be blank!")
        user = self.model(email=self.normalize_email(email), first_name=first_name, last_name=last_name)
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_superuser(self,email,password,first_name,last_name):
        user = self.create_user(email,password,first_name,last_name)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

class CustomUser(AbstractBaseUser):
    email = models.EmailField(max_length=150, unique=True, primary_key=True)
    username = models.CharField(max_length=100, blank=True, null=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_manager = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name','last_name','password']

    objects = CustomUserManager()

    def __str__(self):
        return self.email
    
    def has_perm(self, perm, obj=None):
        return self.is_admin
    
    def has_module_perms(self, app_label):
        return True

class Notebook(models.Model):
    def generate_sku():
        not_unique = True
        while not_unique:
            sku = random.randint(0, 99999999)
            sku = f"{sku:08d}"
            if not Notebook.objects.filter(sku=sku):
                not_unique = False
        return sku

    owner = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True)
    image = models.ImageField(null=True, blank=True)
    sku = models.CharField(default=generate_sku,max_length=8,primary_key=True,unique=True,blank=False)
    brand = models.CharField(max_length=40,blank=False)
    model = models.CharField(max_length=40,blank=False)
    price = models.DecimalField(blank=False,max_digits=7, decimal_places=2)
    screensize = models.CharField(max_length=4,blank=False)
    resolution = models.CharField(max_length=9,blank=False)
    os = models.CharField(max_length=40,blank=False)
    cpufamily = models.CharField(max_length=40,blank=False)
    cpu = models.CharField(max_length=40,blank=False)
    memory = models.IntegerField(blank=False)
    gpu = models.CharField(max_length=40,blank=False)
    hdd = models.IntegerField(null=True,blank=True)
    ssd = models.IntegerField(null=True,blank=True)
    quantity = models.IntegerField(blank=False)
    warranty = models.IntegerField(null=True,blank=True)
    distributor = models.CharField(max_length=40,blank=False)
    adddate = models.DateTimeField(auto_now_add=True)
    rating = models.FloatField(default=0)
    ratingcount = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.sku

class Comment(models.Model):
    notebook = models.ForeignKey(Notebook, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True)
    username = models.CharField(max_length=201)
    comment = models.TextField(null=True, blank=True)
    rating = models.IntegerField(blank=False)
    isaproved = models.BooleanField(default=False)
    adddate = models.DateTimeField(auto_now_add=True)
    

    def __str__(self):
        return str(self.rating)

class Order(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True)
    isdelivered = models.BooleanField(default=False)
    createdate = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return str(self.id)

class OrderItem(models.Model):
    notebook = models.ForeignKey(Notebook, on_delete=models.CASCADE, null=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True)
    quantity = models.IntegerField(blank=False)
    def __str__(self):
        return str(self.id)

class ShippingAdress(models.Model):
    order = models.OneToOneField(Order, on_delete=models.CASCADE)
    city = models.CharField(max_length=150)
    district = models.CharField(max_length=150)
    street = models.CharField(max_length=150)
    postal_code = models.CharField(max_length=5)
    addressdesc = models.TextField()
    def __str__(self):
        return str(self.id)