from django.contrib.auth import authenticate, login, logout
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect

from .forms import *
from .models import *
from .serializers import *

############ USERS ##############

def managersignup(request):
    user = request.user
    if user.is_authenticated:
        return redirect('home')

    if request.method == 'POST':
        form = RegisterationForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password1')
            account = authenticate(email=email,password=password)
            account.is_manager = True
            account.save()
            login(request, account)
            return(JsonResponse({'success':True},status=201))
        return(JsonResponse({'success':False,'errors':form.errors},status=200))
    return(HttpResponse(status=403))

def usersignup(request):
    user = request.user
    if user.is_authenticated:
        return redirect('home')
        
    if request.method == 'POST':
        form = RegisterationForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password1')
            account = authenticate(email=email,password=password)
            login(request, account)
            return(JsonResponse({'success':True},status=201))
        return(JsonResponse({'success':False,'errors':form.errors},status=200))
    return(HttpResponse(status=403))

def userlogout(request):
    if not request.user.is_authenticated:
        return(JsonResponse({'success':False},status=200))
    logout(request)
    return(JsonResponse({'success':True},status=200))

def userlogin(request):
    user = request.user
    if user.is_authenticated:
        return redirect('home')
    if request.POST:
        form = UserAuthenticationForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
            print(email,password)
            account = authenticate(email=email,password=password)
            if account:
                login(request, account)
                return(JsonResponse({'success':True},status=200))
        return(JsonResponse({'success':False,'errors':form.errors},status=200))
    return(HttpResponse(status=403))

def isloggedin(request):
    user = request.user
    if user.is_authenticated:
        return(JsonResponse({'success':True,'email':user.email,'fullname':(request.user.first_name + ' ' + request.user.last_name)},status=200))
    return(JsonResponse({'success':False},status=200))

def ismanager(request):
    user = request.user
    if user.is_authenticated and user.is_manager:
        return(JsonResponse({'success':True,'email':user.email,'fullname':(request.user.first_name + ' ' + request.user.last_name)},status=200))
    return(JsonResponse({'success':False},status=200))

############ PRODUCTS ##############

def getNotebooks(request):
    d = {'datedown':'-adddate','dateup':'adddate','pricedown':'-price','priceup':'price'}
    sortmethod = d[request.GET.get('sort','datedown')]
    query = request.GET.get("q", None)    
    Notebooks = Notebook.objects.all().order_by(sortmethod)
    if query != None:
        # icontains acts like: "SELECT ... WHERE brand ILIKE query;" in SQL 
        Notebooks = Notebooks.filter(
            Q(brand__icontains=query) | Q(model__icontains=query) | Q(cpufamily__icontains=query) | Q(cpu__icontains=query) | Q(gpu__icontains=query)
            )
    Serializer = NotebookSerializer(Notebooks, many=True)
    return JsonResponse(Serializer.data, safe=False, status=200)
        
def getNotebook(request, sku):
    try:
        Notebooks = Notebook.objects.get(pk=sku)
    except Notebook.DoesNotExist:
        return HttpResponse(status=404)
    
    serializer = NotebookSerializer(Notebooks)
    return JsonResponse(serializer.data, safe=False, status=200)

def postNotebook(request):
    tempdata = request.POST.dict()
    im = request.FILES.dict()
    data = {}
    for k,v in tempdata.items():
        if(v != ""):
            data[k] = v
    if(im):
        data['image'] = im['image']
    data['distributor'] = request.user.username
    data['owner'] = request.user
    Serializer = NotebookSerializer(data=data)
    if Serializer.is_valid():
        Serializer.save()
        return HttpResponse(status=201)
    return JsonResponse(Serializer.errors, status=200)

def getComments(request, sku):
    nb = Notebook.objects.get(pk=sku)
    user = request.user
    if user.is_authenticated and user.is_manager and (nb.owner.email == user.email):
        Comments = Comment.objects.filter(notebook=sku).order_by('-adddate')
    else:
        Comments = Comment.objects.filter(notebook=sku).filter(isaproved=True).order_by('-adddate')
    Serializer = CommentSerializer(Comments, many=True)
    return JsonResponse(Serializer.data, safe=False)

def postComment(request, sku):
    data = request.POST.dict()
    data['username'] = request.user.first_name + ' ' + request.user.last_name
    data['notebook'] = sku
    data['user'] = request.user
    Serializer = CommentSerializer(data=data)
    if Serializer.is_valid():
        Serializer.save()
        return HttpResponse(status=201)
    return JsonResponse(Serializer.errors, status=200)

def getFilterKeys(request):
    filterkeys = {}
    filterkeys['brand'] = list(Notebook.objects.values_list('brand',flat=True).distinct())
    filterkeys['screensize'] = list(Notebook.objects.values_list('screensize',flat=True).distinct())
    filterkeys['resolution'] = list(Notebook.objects.values_list('resolution',flat=True).distinct())
    filterkeys['os'] = list(Notebook.objects.values_list('os',flat=True).distinct())
    filterkeys['cpufamily'] = list(Notebook.objects.values_list('cpufamily',flat=True).distinct())
    filterkeys['cpu'] = list(Notebook.objects.values_list('cpu',flat=True).distinct())
    filterkeys['screensize'] = list(Notebook.objects.values_list('screensize',flat=True).distinct())
    filterkeys['gpu'] = list(Notebook.objects.values_list('gpu',flat=True).distinct())
    filterkeys['hdd'] = list(Notebook.objects.values_list('hdd',flat=True).distinct())
    filterkeys['ssd'] = list(Notebook.objects.values_list('ssd',flat=True).distinct())
    return JsonResponse(filterkeys, status=200)

def notebookSearch(request):
    d = {'datedown':'-adddate','dateup':'adddate','pricedown':'-price','priceup':'price'}
    sortmethod = request.GET.get('sort','datedown')
    query = request.GET.get("q", None)    
    Notebooks = Notebook.objects.all().order_by(d[sortmethod])
    if query != None:
        # icontains acts like: "SELECT ... WHERE brand ILIKE query;" in SQL 
        Notebooks = Notebooks.filter(
            Q(brand__icontains=query) | Q(model__icontains=query) | Q(cpufamily__icontains=query) | Q(cpu__icontains=query) | Q(gpu__icontains=query)
            )
    Serializer = NotebookSerializer(Notebooks, many=True)
    return JsonResponse(Serializer.data, safe=False, status=200)

def aproveComment(request,sku,id):
    if request.user.is_authenticated and request.user.is_manager:
        action = request.GET.get('approve',None)
        if action == '1':
            obj = Comment.objects.get(pk=id)
            obj.isaproved = 1
            obj.save()
            nb = Notebook.objects.get(pk=sku)
            nb.rating = (nb.rating * nb.ratingcount + obj.rating) / (nb.ratingcount + 1)
            nb.ratingcount += 1
            nb.save()
            return(JsonResponse({'success':True},status=200))
        elif action == '2':
            Comment.objects.get(pk=id).delete()
            return(JsonResponse({'success':True},status=200))
        return HttpResponse(status=405)
    return HttpResponse(status=403)
