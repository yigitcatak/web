from django.urls import path, include
from api.views import *

from django.conf import settings
from django.conf.urls.static import static

userurls = [
    path('register/', usersignup),
    path('register/manager/', managersignup),
    path('logout/', userlogout),
    path('login/', userlogin), 
    path('isloggedin/', isloggedin),
    path('ismanager/', ismanager),
]

notebookurls = [
    path('', getNotebooks),
    path('search/',notebookSearch),
    path('filterkeys/', getFilterKeys),
    path('create/', postNotebook),
    path('<str:sku>/', getNotebook),
    path('<str:sku>/comments/', getComments),
    path('<str:sku>/comments/<int:id>/', aproveComment),
    path('<str:sku>/comments/create/', postComment),
]

urlpatterns = [
    path('notebooks/',include(notebookurls)),
    path('users/', include(userurls)),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)