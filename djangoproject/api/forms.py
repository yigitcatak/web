from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm

from .models import CustomUser, Notebook


class RegisterationForm(UserCreationForm):
    class Meta:
        model = CustomUser
        fields = ["email","password1","password2","first_name","last_name"]
    
    # def __init__(self, *args, **kwargs):
    #     super(RegisterationForm,self).__init__(*args, **kwargs)
        # del self.fields['username']
        # del self.fields['password2']

class UserAuthenticationForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['email','password']

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        if not authenticate(email=email,password=password):
            raise forms.ValidationError("Invalid login credentials!")
        
class AddNotebookForm(forms.ModelForm):
    class Meta:
        model = Notebook
        fields = ['image', 'sku', 'brand', 'model', 'price', 'screensize', 'resolution', 'os', 'cpufamily', 'cpu', 'memory', 'gpu', 'hdd', 'ssd', 'quantity', 'warranty']
        
        def __init__(self, *args, **kwargs):
            super(AddNotebookForm,self).__init__(*args, **kwargs)
            self.fields['image'].required = False
            self.fields['hdd'].required = False
            self.fields['ssd'].required = False
            self.fields['warranty'].required = False
