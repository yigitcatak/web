import React, { useEffect } from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.css";
import Footer from "./components/Footer";
import Header from "./components/Header";
import AddProduct from "./pages/AddProduct";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import NotFound from "./pages/NotFound";
import ProductPage from "./pages/ProductPage";
import Products from "./pages/Products";
import Register from "./pages/Register";
import axios from "axios";
import { useDispatch } from "react-redux";
import { setAuth } from "./slices/authSlice";

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    async function setAuthorization() {
      const loginresponse = await axios.get(
        "http://127.0.0.1:8000/api/users/isloggedin/"
      );
      if (loginresponse.data.success) {
        const managerresponse = await axios.get(
          "http://127.0.0.1:8000/api/users/ismanager/"
        );
        if (managerresponse.data.success) {
          dispatch(
            setAuth({
              authlevel: 2,
              email: loginresponse.data.email,
              fullname: loginresponse.data.fullname,
            })
          );
        } else {
          dispatch(
            setAuth({
              authlevel: 1,
              email: loginresponse.data.email,
              fullname: loginresponse.data.fullname,
            })
          );
        }
      } else {
        dispatch(
          setAuth({
            authlevel: 0,
            email: null,
            fullname: null,
          })
        );
      }
    }
    setAuthorization();
  }, []);

  return (
    <Router>
      <Header />
      <main className="py-3">
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login/" element={<Login />} />
            <Route path="/logout/" element={<Logout />} />
            <Route path="/register/" element={<Register />} />
            <Route path="/notebooks/" element={<Products />} />
            <Route path="/notebooks/:sku/" element={<ProductPage />} />
            <Route path="/newproduct/" element={<AddProduct />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </Container>
      </main>
      <Footer />
    </Router>
  );
}

export default App;
