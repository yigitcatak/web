import { useEffect } from "react";
import lottie from "lottie-web/build/player/lottie_light";
import animation from "./../lotties/notfound.json";

export default function NotFound() {
  useEffect(() => {
    lottie.loadAnimation({
      container: document.querySelector("#nfd"),
      animationData: animation,
    });
  }, []);
  return (
    <div
      style={{
        display: "flex",
        alignContent: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <div id="nfd" style={{ width: "520px" }} className="center"></div>
      <div className="center" style={{ fontSize: "3em" }}>
        <p>
          <strong>Sorry!</strong> The page you are looking for does not exist
        </p>
      </div>
    </div>
  );
}
