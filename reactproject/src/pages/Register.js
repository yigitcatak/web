import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, useNavigate } from "react-router-dom";
import CSRFToken from "../components/CSRF";
import { setAuth } from "../slices/authSlice";

function Register() {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    let config = {
      headers: {
        "X-CSRFToken": data.get("csrfmiddlewaretoken"),
      },
    };
    axios
      .post(`http://127.0.0.1:8000/api/users/register/`, data, config)
      .then(function (response) {
        if (response.data.success === true) {
          async function setAuthorization() {
            const loginresponse = await axios.get(
              "http://127.0.0.1:8000/api/users/isloggedin/"
            );
            const managerresponse = await axios.get(
              "http://127.0.0.1:8000/api/users/ismanager/"
            );
            if (managerresponse.data.success) {
              dispatch(
                setAuth({
                  authlevel: 2,
                  email: loginresponse.data.email,
                  fullname: loginresponse.data.fullname,
                })
              );
            } else {
              dispatch(
                setAuth({
                  authlevel: 1,
                  email: loginresponse.data.email,
                  fullname: loginresponse.data.fullname,
                })
              );
            }
            navigate("/");
          }
          setAuthorization();
        } else {
          console.log(response.data);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  if (auth.authlevel !== 0) {
    return <Navigate to="/" />;
  }
  return (
    <div className="page-root">
      <form onSubmit={(e) => handleSubmit(e)}>
        <CSRFToken />
        <p>
          <label>
            email:
            <input type="text" name="email" />
          </label>
        </p>
        <p>
          <label>
            password:
            <input type="password" name="password1" />
          </label>
        </p>
        <p>
          <label>
            password2:
            <input type="password" name="password2" />
          </label>
        </p>
        <p>
          <label>
            first name:
            <input type="text" name="first_name" />
          </label>
        </p>
        <p>
          <label>
            last name:
            <input type="text" name="last_name" />
          </label>
        </p>
        <input type="submit" value="Register!" />
      </form>
    </div>
  );
}

export default Register;
