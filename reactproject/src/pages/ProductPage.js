import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Row, Col, Image, ListGroup, Button, Form } from "react-bootstrap";
import ShowRating from "../components/ShowRating";
import Comments from "../components/Comments";
import axios from "axios";

function ProductPage() {
  const [qty, setQty] = useState();
  const [product, setProduct] = useState([]);
  const { sku } = useParams();

  useEffect(() => {
    async function fetchProduct() {
      const { data } = await axios.get(
        `http://127.0.0.1:8000/api/notebooks/${sku}/`
      );
      setProduct(data);
    }
    fetchProduct();
  }, [sku]);

  return (
    <>
      <Row>
        <Col md={6}>
          <Image
            src={"http://127.0.0.1:8000/static" + product.image}
            style={{ width: "100%", height: "100%" }}
            fluid
          />
        </Col>
        <Col md={6}>
          <ListGroup variant="flush" className="product-detail">
            <ListGroup.Item>
              <h2>{product.brand + " " + product.model}</h2>
            </ListGroup.Item>
            <ListGroup.Item>
              <div className="rating">
                <ShowRating value={product.rating} />
                <span style={{ marginLeft: "1rem" }}>
                  {`${product.ratingcount} reviews`}
                </span>
              </div>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>Price:</Col>
                <Col>{product.price} TL</Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>CPU:</Col>
                <Col>{product.cpufamily + " " + product.cpu}</Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>Memory:</Col>
                <Col>{product.memory + "GB "}</Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>SSD:</Col>
                <Col>
                  {product.ssd
                    ? product.ssd < 11
                      ? product.ssd + "TB"
                      : product.ssd + "GB"
                    : "-"}
                </Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>HDD:</Col>
                <Col>
                  {product.hdd
                    ? product.hdd < 11
                      ? product.hdd + "TB"
                      : product.hdd + "GB"
                    : "-"}
                </Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>GPU:</Col>
                <Col>{product.gpu}</Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>OS:</Col>
                <Col>{product.os}</Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>Warranty:</Col>
                <Col>
                  {product.warranty ? product.warranty + " years" : "-"}
                </Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>Seller:</Col>
                <Col>{product.distributor}</Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>OS:</Col>
                <Col>{product.os}</Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>
              <Row>
                <Col>Stock:</Col>
                <Col>
                  {product.quantity > 0 ? product.quantity : "Out of Stock"}
                </Col>
              </Row>
            </ListGroup.Item>
            {product.quantity > 0 && (
              <ListGroup.Item>
                <Row>
                  <Col className="d-flex align-items-center">Quantity:</Col>
                  <Col>
                    <select
                      className="form-select"
                      as="select"
                      value={qty}
                      onChange={(e) => setQty(e.target.value)}
                    >
                      {[...Array(product.quantity).keys()].map((x) => (
                        <option key={x + 1} value={x + 1}>
                          {" "}
                          {x + 1}{" "}
                        </option>
                      ))}
                    </select>
                  </Col>
                </Row>
              </ListGroup.Item>
            )}
            <Button
              style={{ width: "100%" }}
              className="btn-block"
              disabled={product.quantity === 0}
              type="button"
            >
              ADD TO CART
            </Button>
          </ListGroup>
        </Col>
      </Row>
      <Row>
        <Comments sku={sku} />
      </Row>
    </>
  );
}

export default ProductPage;
