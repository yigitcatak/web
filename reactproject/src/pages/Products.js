import React, { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import ProductCard from "../components/ProductCard";
import axios from "axios";
import { Link } from "react-router-dom";

function Products() {
  const [products, setProducts] = useState([]);
  const [sortparam, setSortparam] = useState(null);
  const [query, setQuery] = useState(null);

  useEffect(() => {
    async function fetchNotebooks() {
      let sortparam = new URL(window.location).searchParams.get("sort");
      if (!sortparam) {
        sortparam = "datedown";
      }
      let query = new URL(window.location).searchParams.get("q");
      const url = `http://127.0.0.1:8000/api/notebooks/?sort=${sortparam}${
        query ? "&q=" + query : ""
      }`;
      const { data } = await axios.get(url);
      setSortparam(sortparam);
      setQuery(query);
      setProducts(data);
    }
    fetchNotebooks();
  }, [query,sortparam]);

  async function sortButtonHandle(sortparam) {
    const { data } = await axios.get(
      `http://127.0.0.1:8000/api/notebooks/?sort=${sortparam}${
        query ? "&q=" + query : ""
      }`
    );
    setSortparam(sortparam);
    setProducts(data);
  }

  if (products.length === 0) {
    return <h3>No product found!</h3>
  }

  return (
    <div id="list-area">
      <div className="shoplist-sortbar">
        <Link
          to={"?sort=datedown" + (query ? "&q=" + query : "")}
          onClick={() => sortButtonHandle("datedown")}
          className={
            sortparam === "datedown"
              ? "sort-button sort-button-selected"
              : "sort-button"
          }
        >
          {" "}
          Newest to Oldest{" "}
        </Link>
        <Link
          to={"?sort=dateup" + (query ? "&q=" + query : "")}
          onClick={() => sortButtonHandle("dateup")}
          className={
            sortparam === "dateup"
              ? "sort-button sort-button-selected"
              : "sort-button"
          }
        >
          {" "}
          Oldest to Newest{" "}
        </Link>
        <Link
          to={"?sort=priceup" + (query ? "&q=" + query : "")}
          onClick={() => sortButtonHandle("priceup")}
          className={
            sortparam === "priceup"
              ? "sort-button sort-button-selected"
              : "sort-button"
          }
        >
          {" "}
          Increasing Price{" "}
        </Link>
        <Link
          to={"?sort=pricedown" + (query ? "&q=" + query : "")}
          onClick={() => sortButtonHandle("pricedown")}
          className={
            sortparam === "pricedown"
              ? "sort-button sort-button-selected"
              : "sort-button"
          }
        >
          {" "}
          Decreasing Price{" "}
        </Link>
      </div>
      <Row>
        {products.map((product) => (
          <Col sm={12} md={12} lg={6} xl={4} key={product.sku} className="my-3">
            <ProductCard product={product} />
          </Col>
        ))}
      </Row>
    </div>
  );
}

export default Products;
