import axios from "axios";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, useNavigate } from "react-router-dom";
import { setAuth } from "../slices/authSlice";

function Logout() {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const navigate = useNavigate();

  useEffect(() => {
    async function checkLogoutStatus() {
      const { data } = await axios.get(
        "http://127.0.0.1:8000/api/users/logout/"
      );
      if (data.success) {
        dispatch(
          setAuth({
            authlevel: 0,
            email: null,
            fullname: null,
          })
        );
        navigate("/");
      }
    }
    checkLogoutStatus();
  }, []);

  if (auth.authlevel === 0) {
    <Navigate to="/" />;
  }
  return <h3> You are not Loggedin! </h3>;
}
export default Logout;
