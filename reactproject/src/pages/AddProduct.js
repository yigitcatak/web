import axios from "axios";
import React, { useEffect, useState } from "react";
import CSRFToken from "../components/CSRF";

function AddProduct() {
  const [isLoggedin, setLoginStatus] = useState(false);
  const [isManager, setManagerStatus] = useState(false);

  useEffect(() => {
    async function checkLoginStatus() {
      const loginresponse = await axios.get(
        "http://127.0.0.1:8000/api/users/isloggedin/"
      );
      setLoginStatus(loginresponse.data.success);
      const managerresponse = await axios.get(
        "http://127.0.0.1:8000/api/users/ismanager/"
      );
      setManagerStatus(managerresponse.data.success);
    }
    checkLoginStatus();
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    let config = {
      headers: {
        "X-CSRFToken": data.get("csrfmiddlewaretoken"),
      },
    };
    axios
      .post("http://127.0.0.1:8000/api/notebooks/create/", data, config)
      .then(function (response) {
        if (response.status === 201) {
          alert("Your product is created!");
        } else {
          console.log(response.data);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  if (!isLoggedin) {
    return (
      <>
        <h3>Please login with your manager account to add products!</h3>
      </>
    );
  } else if (!isManager) {
    return (
      <h3>
        This account is not authorized to add products! If you have a manager
        account, please login with that.
      </h3>
    );
  }
  return (
    <div className="page-root" onSubmit={(e) => handleSubmit(e)}>
      <form id="productform">
        <CSRFToken />
        <p>
          <label>Brand:</label>
          <input type="text" name="brand" />
        </p>
        <p>
          <label>Model:</label>
          <input type="text" name="model" />
        </p>
        <p>
          <label>Price:</label>
          <input type="number" name="price" />
        </p>
        <p>
          <label>Screen Size:</label>
          <input type="text" name="screensize" />
        </p>
        <p>
          <label>Resolution:</label>
          <input type="text" name="resolution" />
        </p>
        <p>
          <label>Operating System:</label>
          <input type="text" name="os" />
        </p>
        <p>
          <label>CPU Family:</label>
          <input type="text" name="cpufamily" />
        </p>
        <p>
          <label>CPU Model:</label>
          <input type="text" name="cpu" />
        </p>
        <p>
          <label>Memory:</label>
          <input type="number" name="memory" />
        </p>
        <p>
          <label>GPU:</label>
          <input type="text" name="gpu" />
        </p>
        <p>
          <label>HDD size:</label>
          <input type="number" name="hdd" />
        </p>
        <p>
          <label>SSD size:</label>
          <input type="number" name="ssd" />
        </p>
        <p>
          <label>Stock quantity:</label>
          <input type="number" name="quantity" />
        </p>
        <p>
          <label>Warranty duration:</label>
          <input type="number" name="warranty" />
        </p>
        <p>
          <label>Image:</label>
          <input type="file" name="image" />
        </p>
        <input type="submit" value="Register Product" />
      </form>
    </div>
  );
}
export default AddProduct;
