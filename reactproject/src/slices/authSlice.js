import { createSlice } from "@reduxjs/toolkit";

export const authSlice = createSlice({
  name: "auth",
  initialState: {
    authlevel: 0,
    fullname: null,
    email: null,
  },
  reducers: {
    setAuth: (state, action) => {
      state.authlevel = action.payload.authlevel;
      state.fullname = action.payload.fullname;
      state.email = action.payload.email;
    },
  },
});

export const { setAuth } = authSlice.actions;
export default authSlice.reducer;
