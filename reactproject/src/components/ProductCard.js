import React from "react";
import { Card } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import ShowRating from "./ShowRating";

function ProductCard({ product }) {
  return (
    <LinkContainer
      className="product-card"
      to={`/notebooks/${product.sku}`}
      title={
        product.brand +
        " " +
        product.model +
        " " +
        product.cpufamily +
        " " +
        product.cpu +
        " " +
        product.memory +
        "GB " +
        (product.ssd
          ? product.ssd < 11
            ? product.ssd + "TB SSD "
            : product.ssd + "GB SSD "
          : "") +
        (product.hdd
          ? product.hdd < 11
            ? product.hdd + "TB HDD "
            : product.hdd + "GB HDD "
          : "") +
        product.gpu +
        " " +
        product.os
      }
    >
      <Card className="rounded h-100">
        <Card.Img
          src={"http://127.0.0.1:8000/static" + product.image}
          style={{ height: "100%", width: "100%", minHeight: "400px" }}
        />
        <Card.Body>
          <Card.Title as="div">
            <strong>
              {product.brand +
                " " +
                product.model +
                " " +
                product.cpufamily +
                " " +
                product.cpu +
                " " +
                product.memory +
                "GB " +
                (product.ssd
                  ? product.ssd < 11
                    ? product.ssd + "TB SSD "
                    : product.ssd + "GB SSD "
                  : "") +
                (product.hdd
                  ? product.hdd < 11
                    ? product.hdd + "TB HDD "
                    : product.hdd + "GB HDD "
                  : "") +
                product.gpu +
                " " +
                product.os}
            </strong>
          </Card.Title>
          <Card.Text as="div">
            <div className="my-3">
              <div className="rating">
                <ShowRating value={product.rating} />
                <span style={{ marginLeft: "1rem" }}>
                  {`${product.ratingcount} reviews`}
                </span>
              </div>
            </div>
          </Card.Text>
          <Card.Text as="h3">{product.price} TL</Card.Text>
        </Card.Body>
      </Card>
    </LinkContainer>
  );
}

export default ProductCard;
