import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import CSRFToken from "../components/CSRF";
import CommentCard from "./CommentCard";
import ShowRating from "./ShowRating";

function Comments({ sku }) {
  const [comments, setData] = useState([]);
  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    async function fetchComments() {
      const { data } = await axios.get(
        `http://127.0.0.1:8000/api/notebooks/${sku}/comments/`
      );
      setData(data);
    }
    fetchComments();
  },[]);

  const newComment = (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    let config = {
      headers: {
        "X-CSRFToken": data.get("csrfmiddlewaretoken"),
      },
    };
    axios
      .post(
        `http://127.0.0.1:8000/api/notebooks/${sku}/comments/create/`,
        data,
        config
      )
      .then(function (response) {
        if (response.status === 201) {
          alert("Your comment is created!");
        } else {
          console.log(response.data);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  if (auth.authlevel === 0) {
    return (
      <>
        {comments.map((comment) => (
          <CommentCard comment={comment}/>
        ))}
      </>
    );
  }
  return (
    <>
      <div onSubmit={(e) => newComment(e)} style={{marginTop:'1rem', width:"100%"}}>
        <form id="commentform" autoComplete="off" style={{width:"100%"}}>
          <CSRFToken />
          <p>
            <label>Comment:</label>
            <input type="text" name="comment" style={{height:'3em', width:"100%"}}/>
          </p>
          <p>
            <label>Rating:</label>
            <input type="number" name="rating" min="1" max="5"/>
          </p>
          <input type="submit" value="Send Comment" />
        </form>
      </div>
      {comments.map((comment) => (
        <CommentCard comment={comment}/>
      ))}
    </>
  );
}

export default Comments;
