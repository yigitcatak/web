import axios from "axios";
import { useState } from "react";
import ShowRating from "./ShowRating";

function CommentCard({ comment }) {
  const [notActive, setActive] = useState(false);
  function approveHandle() {
    axios.get(
      `http://127.0.0.1:8000/api/notebooks/${comment.notebook}/comments/${comment.id}/?approve=1`
    );
    setActive(true)
  }
  function deleteHandle() {
    axios.get(
      `http://127.0.0.1:8000/api/notebooks/${comment.notebook}/comments/${comment.id}/?approve=2`
    );
    setActive(true)
  }
  if (comment.isaproved) {
    return (
      <div style={{ marginTop: "1rem" }}>
        <div style={{ display: "inline-block" }}>{comment.username}</div>
        <div style={{ display: "inline-block", marginLeft: "0.5rem" }}>
          <ShowRating value={comment.rating} />
        </div>
        <div>{comment.comment}</div>
      </div>
    );
  }
  return (
    <div style={{ marginTop: "1rem"}}>
      <div>
        <button onClick={approveHandle} disabled={notActive}>Approve</button>
        <button onClick={deleteHandle} disabled={notActive}>Delete</button>
      </div>
      <div style={{ display: "inline-block" }}>{comment.username}</div>
      <div style={{ display: "inline-block", marginLeft: "0.5rem" }}>
        <ShowRating value={comment.rating} />
      </div>
      <div>{comment.comment}</div>
    </div>
  );
}

export default CommentCard;
