import { useNavigate } from "react-router-dom";

function Searchbar() {
  const navigate = useNavigate();
  const searchHandle = (e) => {
    e.preventDefault();
    let sortparam = new URL(window.location).searchParams.get("sort");
    if (!sortparam) {
      sortparam = "datedown";
    }
    let query = e.target.query.value;
    navigate(`/notebooks/?sort=${sortparam}${query ? "&q=" + query : ""}`);
  };

  return (
      <form onSubmit={(e) => searchHandle(e)}>
        <i class="fa-solid fa-magnifying-glass"></i>
        <input style={{ marginLeft: "1rem", width: "18em"}} type="text" name="query" />
      </form>
  );
}

export default Searchbar;
