import axios from "axios";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setAuth } from "../slices/authSlice";

export default function Authorization() {
  const dispatch = useDispatch();
  useEffect(() => {
    async function setAuthorization() {
      const loginresponse = await axios.get(
        "http://127.0.0.1:8000/api/users/isloggedin/"
      );
      if (loginresponse.data.success) {
        const managerresponse = await axios.get(
          "http://127.0.0.1:8000/api/users/ismanager/"
        );
        if (managerresponse.data.success) {
          dispatch(
            setAuth({
              authlevel: 2,
              email: loginresponse.data.email,
              fullname: loginresponse.data.fullname,
            })
          );
        } else {
          dispatch(
            setAuth({
              authlevel: 1,
              email: loginresponse.data.email,
              fullname: loginresponse.data.fullname,
            })
          );
        }
      } else {
        dispatch(
          setAuth({
            authlevel: 0,
            email: null,
            fullname: null,
          })
        );
      }
    }
    setAuthorization();
  }, []);
}

async function setAuthorization() {
  const dispatch = useDispatch();
  const loginresponse = await axios.get(
    "http://127.0.0.1:8000/api/users/isloggedin/"
  );
  if (loginresponse.data.success) {
    const managerresponse = await axios.get(
      "http://127.0.0.1:8000/api/users/ismanager/"
    );
    if (managerresponse.data.success) {
      dispatch(
        setAuth({
          authlevel: 2,
          email: loginresponse.data.email,
          fullname: loginresponse.data.fullname,
        })
      );
    } else {
      dispatch(
        setAuth({
          authlevel: 1,
          email: loginresponse.data.email,
          fullname: loginresponse.data.fullname,
        })
      );
    }
  } else {
    dispatch(
      setAuth({
        authlevel: 0,
        email: null,
        fullname: null,
      })
    );
  }
}
