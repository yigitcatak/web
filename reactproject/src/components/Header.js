import React from 'react'
import { useSelector } from "react-redux";
import Searchbar from './Searchbar';
import {Link} from "react-router-dom"

function Header() {
  const loginstatus = useSelector(state => state.auth)

  if( loginstatus.authlevel === 0){
    return (
        <header>
            <div className='myheader-root'>
                <div className='myheader-body'>
                    <div className='myheader-hoverable'><Link to='/' style={{fontSize:"2em", marginRight:"0.5rem"}}>Team 06</Link></div>
                    <div className='myheader-hoverable'><Link to='/notebooks/' >Notebooks</Link></div>
                    <div className='myheader-hoverable'><Link to='/cart/' > <i className='fas fa-shopping-cart'></i>Cart</Link></div>
                    <div><Searchbar/></div>
                    <div className='myheader-fill'></div>
                    <div className='myheader-hoverable'><Link to='/login/'><i className='fas fa-user'></i>Login</Link></div>
                    <div className='myheader-hoverable'><Link to='/register/' >Register</Link></div>
                </div>
            </div>
        </header>
    )
  } else if( loginstatus.authlevel === 1){
    return (
        <header>
            <div className='myheader-root'>
                <div className='myheader-body'>
                    <div className='myheader-hoverable'><Link to='/' style={{fontSize:"2em", marginRight:"0.5rem"}}>Team 06</Link></div>
                    <div className='myheader-hoverable'><Link to='/notebooks/' >Notebooks</Link></div>
                    <div className='myheader-hoverable'><Link to='/cart/' > <i className='fas fa-shopping-cart'></i>Cart</Link></div>
                    <div><Searchbar/></div>
                    <div className='myheader-fill'></div>
                    <div className='myheader-hoverable'><Link to='/logout/' >Logout</Link></div>
                </div>
                <div className='myheader-body'>
                    <div className='myheader-fill'></div>
                    <div>user: {loginstatus.fullname}</div>
                </div>
            </div>
        </header>
    )
  } else {
    return (
        <header>
            <div className='myheader-root'>
                <div className='myheader-body'>
                    <div className='myheader-hoverable'><Link to='/' style={{fontSize:"2em", marginRight:"0.5rem"}}>Team 06</Link></div>
                    <div className='myheader-hoverable'><Link to='/notebooks/' >Notebooks</Link></div>
                    <div className='myheader-hoverable'><Link to='/cart/' > <i className='fas fa-shopping-cart'></i>Cart</Link></div>
                    <div><Searchbar/></div>
                    <div className='myheader-fill'></div>
                    <div className='myheader-hoverable'><Link to='/newproduct/' >Add Product</Link></div>
                    <div className='myheader-hoverable'><Link to='/logout/' >Logout</Link></div>
                </div>
                <div className='myheader-body'>
                    <div className='myheader-fill'></div>
                    <div>user: {loginstatus.fullname}</div>
                </div>
            </div>
        </header>
    )
  } 
}

export default Header